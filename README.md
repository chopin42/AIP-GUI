# AIP Visual
Install any app on Linux without using terminal!

![Screenshot of the result here](finished.png)

Install any app on Linux without using terminal! You just need to drag'n'drop text or files and they will be installed directly, this is even easier than installing software in Windows.

## Requirements
This project needs few requirements:

* Git
* Node and NPM (optional)
* Xterm

You can also set yourself into the sudoders list and removing the terminal option in aip-gui.desktop file.

## Build from source
To build it from source you just need to run the following commands: (Maybe your last commands ;)

```
git clone https://gitea.com/chopin42/AIP-GUI
cd AIP-GUI
sudo sh setup.sh
```

## Troubleshooting
If you got an issue, search into the [issues](https://gitea/chopin42/AIP-GUI/issues) to find your issue. If not exist yet then, create one and wait for answer by the community and developers. If this issue is caused by a bug it will be solved in the next release. Thanks for contributing to this project!

## Contribute!

You want to contribute? Thanks! You can get all the information at [CONTRIBUTING guide](./CONTRIBUTING.md)

## Meta
This project has been created by SnowCode, thanks to all contributors to make this possible!

This project is under GNU GENERAL PUBLIC LICENSE, to get more information go to [LICENSE file](./LICENSE)
