//Modules installed along the way
const fs = require('fs');
const filePath = 'testFile.txt';
const child_process = require('child_process');

function code_submit(e){
  //Get the values
  var code_fence = document.getElementById('code_fence');
  var code = code_fence.value;

  //Create a new sh file called "install.sh"
  fs.writeFile("install.sh", code, (err => {
    if(err){
      alert("An error ocurred: " + err.message)
    }
  }))

  //Run the file in a new terminal with sudo
  child_process.exec("xterm -hold -e 'sudo sh install.sh && echo Done!'");
  console.log("xterm -hold -e 'sudo sh install.sh && echo Done!'");
}

function drop(e){
  var holder = document.getElementById('drag-file');

  //What happen when nothing
  holder.ondragover = () => {
      return false;
  };

  holder.ondragleave = () => {
      return false;
  };

  holder.ondragend = () => {
      return false;
  };

  //What happen when dropped
  holder.ondrop = (e) => {
    e.preventDefault();
    for (let f of e.dataTransfer.files) {
      //Get the filename
      let file = f.path;

      //Find the type of file
      let ext = file.split('.').pop();
      //Check the different types and run the appropriated lines
      if (ext == "deb") {
        console.log("Deb package detected");
        child_process.exec("xterm -hold -e 'sudo dpkg -i " + file + " && sudo apt-get -f install && echo Done!'");
      }
      else if (ext == "jar") {
        console.log("Java file detected");
        child_process.exec("xterm -hold -e 'sudo java -jar " + file + " && echo Done!'");
      }
      else if ((ext == "AppImage") || (ext == "run")) {
        console.log("Binary file detected");
        child_process.exec("xterm -hold -e 'sudo chmod +x " + file + " && sudo " + file + " && echo Done!'");
      }
      else if (ext == "sh") {
        console.log("Shell Script file detected");
        child_process.exec("xterm -hold -e 'sudo chmod +x " + file + " && sudo sh " + file + " && echo Done!'")
      }
      else if (ext == "exe") {
        console.log("FATAL: Unsupported file: exe");
        alert("The exe format is a Windows format AIP doesn't support it yet.")
      }
      else {
        console.log("ERROR: Unrecognized file. Trying to use it as a binary");
        child_process.exec("xterm -hold -e 'sudo chmod +x " + file + " && sudo " + file + " && echo Done!'");
        alert("WARNING: If the installation didn't worked, try to check your file, if your file is really an installer, then please report an issue.")
      }
    }
    return false;
  }
}
