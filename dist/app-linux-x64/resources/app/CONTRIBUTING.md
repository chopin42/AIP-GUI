# Contributing to AIP-GUI

:+1::tada: First off, thanks for taking the time to contribute! :tada::+1:

We've put together the following guidelines to help you figure out where you can best be helpful.

Use your best judgment, and feel free to propose changes to this document in a pull request.

## Table of Contents

0. [Types of contributions we're looking for](#types-of-contributions-were-looking-for)
0. [Expectations](#expectations)
0. [How to contribute](#how-to-contribute)
0. [Setting up your environment](#setting-up-your-environment)
0. [Community](#community)

## Types of contributions we're looking for
There are many ways you can directly contribute (in descending order of need):

* CSS styling to get a better interface
* Bug solving
* Reviews
* New functionalities
* New file types

Interested in making a contribution? Read on!

## Expectations

Before we get started, here are a few things we expect from you (and that you should expect from others):

* Open Source Guides are released with a [Contributor Code of Conduct](./CODE-OF-CONDUCT.md). By participating in this project, you agree to abide by its terms.
* Code need to be commented to be understandable by everybody.
* Rule 2


## How to contribute

If you'd like to contribute, start by searching through the [issues](https://gitea.com/chopin42/AIP-GUI/issues) and [pull requests](https://gitea.com/chopin42/AIP-GUI/pulls) to see whether someone else has raised a similar idea or question.

If you don't see your idea listed, and you think it fits into the goals of this guide, do one of the following:
* **If your contribution is minor,** such as a CSS styling, open a pull request.
* **If your contribution is major,** such as a new functionalities, start by opening an issue first. That way, other people can weigh in on the discussion before you do any work.

## Setting up your environment
To work on the same project you need to clone the project and getting npm and electron installed as well as Xterm terminal. I recommend you to use the Atom Code Editor to work in the project. I also recommend you to use the "atom-html-preview" module on Atom.

## Community

Discussions about the project take place on this repository's [Issues](https://gitea.com/chopin42/AIP-GUI/issues) and [Pull Requests](https://gitea.com/chopin42/AIP-GUI/pulls) sections. Anybody is welcome to join these conversations. There is also a [mailing list](EMAILLISTURL) for regular updates.

Wherever possible, do not take these conversations to private channels, including contacting the maintainers directly. Keeping communication public means everybody can benefit and learn from the conversation. But if you got a **major security issue** maybe try to send your issue in private to developers via : YOUREMAILHERE
