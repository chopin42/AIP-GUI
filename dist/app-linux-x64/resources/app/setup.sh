# Installing Xterm
sudo apt install xterm

#Creating a new directory
mkdir /opt/AIP-GUI
cp -r ./dist/*/* /opt/AIP-GUI
cp aip-gui.desktop /usr/share/applications
chmod 777 -R /opt/AIP-GUI
chmod 777 -R /opt/AIP-GUI/*

#Allow the run file
chmod +x /opt/AIP-GUI/app
